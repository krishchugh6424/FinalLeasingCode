import React, { useState, useEffect } from 'react';

function PayDetailsForm({ prevStep, onChange, data, onSubmit }) {
  const [payData, setPayData] = useState({
    annualSalary: data.annualSalary || '',
    payFrequency: data.payFrequency || 'Weekly',
    carAllowance: data.carAllowance || false
  });

  useEffect(() => {
    onChange(payData);
  }, [payData]);

  const handleBackClick = (e) => {
    e.preventDefault();
    prevStep();
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit();
  };

  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;
    setPayData({ ...payData, [name]: type === 'checkbox' ? checked : value });
  };

  return (
    <div className="mb-4">
      <h2 className="text-lg font-semibold mb-4">Pay Details</h2>
      <form onSubmit={handleSubmit}>
        <div className="grid grid-cols-1 gap-4 mb-4">
          <div>
            <label className="block text-sm font-medium text-gray-700">Annual Gross Salary</label>
            <input
              type="number"
              name="annualSalary"
              value={payData.annualSalary}
              onChange={handleChange}
              className="mt-1 block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
            />
          </div>
          <div>
            <label className="block text-sm font-medium text-gray-700">Select your Pay Frequency</label>
            <div className="flex space-x-4 mt-1">
              {['Weekly', 'Fortnightly', 'Monthly'].map((frequency) => (
                <button
                  key={frequency}
                  type="button"
                  name="payFrequency"
                  onClick={() => setPayData({ ...payData, payFrequency: frequency })}
                  className={`inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white ${payData.payFrequency === frequency ? 'bg-blue-600' : 'bg-gray-600'} hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500`}
                >
                  {frequency}
                </button>
              ))}
            </div>
          </div>
          <div>
            <label className="block text-sm font-medium text-gray-700">
              <input
                type="checkbox"
                name="carAllowance"
                checked={payData.carAllowance}
                onChange={handleChange}
                className="mr-2 leading-tight"
              />
              I have a car allowance
            </label>
          </div>
        </div>
        <div className="flex justify-between">
          <button
            type="button"
            onClick={prevStep}
            className="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-gray-600 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500"
          >
            Back
          </button>
          <button
            type="submit"
            className="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
          >
            Submit
          </button>
        </div>
      </form>
    </div>
  );
}

export default PayDetailsForm;
