import React, { useState, useEffect } from 'react';
import Header from './components/Header';
import CarInfoForm from './components/carInfoForm';
import AdditionalDetailsForm from './components/AdditionalDetailsForm';
import PayDetailsForm from './components/PayDetailsForm';
// import CarImage from './assets/tin.svg';
import './App.css';

function App() {
  const [step, setStep] = useState(() => {
    const savedStep = localStorage.getItem('currentStep');
    return savedStep !== null ? parseInt(savedStep, 10) : 0;
  });

  const [formData, setFormData] = useState(() => {
    const savedData = localStorage.getItem('formData');
    return savedData ? JSON.parse(savedData) : {
      manufacturer: '',
      model: '',
      year: '',
      bodyType: '',
      series: '',
      vehiclePrice: '',
      annualDistance: '',
      state: '',
      postcode: '',
      annualSalary: '',
      payFrequency: 'Weekly',
      carAllowance: false
    };
  });

  useEffect(() => {
    localStorage.setItem('currentStep', step);
  }, [step]);

  useEffect(() => {
    localStorage.setItem('formData', JSON.stringify(formData));
  }, [formData]);

  const nextStep = () => {
    setStep((prevStep) => prevStep + 1);
  };

  const prevStep = () => {
    setStep((prevStep) => prevStep - 1);
  };

  const handleFormDataChange = (newData) => {
    setFormData((prevData) => ({ ...prevData, ...newData }));
  };

  const handleSubmit = () => {
    console.log("Submitted Data: ", formData);
  };

  return (
    <div className="bg-indigo-100 flex justify-center items-start min-h-screen py-8">
      <div className="bg-white w-full max-w-7xl p-9 rounded-lg shadow-lg transition-all duration-500">
        <div className="flex justify-center mb-8">
          {/* <img src={CarImage} alt="Car Leasing" className="bg-gradient-to-t" /> */}
        </div>
        <Header />
        <div className="transition-all duration-500">
          {step === 0 && <CarInfoForm nextStep={nextStep} onChange={handleFormDataChange} data={formData} />}
          {step === 1 && <AdditionalDetailsForm nextStep={nextStep} prevStep={prevStep} onChange={handleFormDataChange} data={formData} />}
          {step === 2 && <PayDetailsForm prevStep={prevStep} onChange={handleFormDataChange} data={formData} onSubmit={handleSubmit} />}
        </div>
      </div>
    </div>
  );
}

export default App;
